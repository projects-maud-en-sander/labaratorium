<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Je wachtwoord is gereset!',
    'sent' => 'er is een email gestuurd voor het resetten van uw wachtwoord',
    'throttled' => 'Wacht 30 seconden voordat u het weer probeerd',
    'token' => 'Deze reset token is ongeldig.',
    'user' => "Er is geen gebruiker gevonden met dit email ",

];
