@extends('layouts.home')

@section('head')
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="jumbotron"></div>

    <div class="container">
        <div class="row justify-content-center">
            @foreach($notifications as $notification)
                <div class="m-2 col-lg-3 col-sm-12 d-flex align-items-stretch">
                    <div class=" card rounded-0" style="border-color: #45B97C; border-width: 1px; padding-bottom: 50px;">
                        <div class="card-body">
                            <h5 class="card-title">{{ $notification->title }}</h5>

                            {{ $notification->message }}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
