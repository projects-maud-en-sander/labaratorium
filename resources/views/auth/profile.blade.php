@extends('layouts.app')

@section('head')
    <link href="{{ asset('css/profile.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container mt-2" >
        <h2>Profiel pagina</h2>
        <table class="table">
            <tbody>
            <tr>
                <th scope="row">Naam</th>
                <td>{{ Auth::user()->name }}</td>
            </tr>
            <tr>
                <th scope="row">Student nr</th>
                <td>{{ Auth::user()->student_number }}</td>
            </tr>
            <tr>
                <th scope="row">Email</th>
                <td>{{ Auth::user()->email }}</td>
            </tr>
            <tr>
                <th scope="row">Niveau</th>
                <td>{{ Auth::user()->level }}</td>
            </tr>
            </tbody>
        </table>

        <form action="{{ route('profile.edit', Auth::User()) }}" method="GET">
            <button type="submit" class="btn btn-secondary">Wijzigen</button>
        </form>
    </div>
@endsection

