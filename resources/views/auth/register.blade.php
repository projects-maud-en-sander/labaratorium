@extends('layouts.app')

@section('head')
    <link href="{{ asset('css/register.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="card" style="border-color: #45B97C; border-width: 1px">
                    <div class="card-body">
                        <div class="text-center"
                             style="color: #45B97C; font-size: 45px; font-weight: bold; font-family: 'Segoe UI'">
                            Registreer
                        </div>
                        <br>
                        <!-- This is the register form you have to enter a name, a student number, a school email, a password,
                             a password repeat and your role here if invalid the form will show the according error messages -->
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class="form-group row pb-0 mb-0">
                                <label for="name" class="col col-form-label"
                                       style="font-weight: bold">{{ __('Naam:') }}</label>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror" name="name"
                                           value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row pb-0 mb-0">
                                <label for="student_number" class="col col-form-label"
                                       style="font-weight: bold">{{ __('Leerling nummer:') }}</label>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <input id="student_number" type="text"
                                           class="form-control @error('student_number') is-invalid @enderror"
                                           name="student_number" value="{{ old('student_number') }}"
                                           autocomplete="student_number" autofocus>

                                    @error('student_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row pb-0 mb-0">
                                <label for="email" class="col col-form-label"
                                       style="font-weight: bold">{{ __('E-Mail:') }}</label>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror" name="email"
                                           value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row pb-0 mb-0">
                                <label for="password" class="col col-form-label"
                                       style="font-weight: bold">{{ __('Wachtwoord:') }}</label>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <input id="password" type="password"
                                           class="form-control @error('password') is-invalid @enderror" name="password"
                                           required autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>

                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row pb-0 mb-0">
                                <label for="password-confirm" class="col col-form-label"
                                       style="font-weight: bold">{{ __('Herhaal wachtwoord:') }}</label>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <input type="hidden" name="roles_id" value="1">

                            <div class="form-group row mb-0">
                                <div class="col offset-md-9">
                                    <button type="submit" class="btn rounded-0"
                                            style="background-color: #45B97C; color: white">
                                        {{ __('Registreer') }}
                                    </button>
                                </div>
                            </div>
                            <p class="mt-2">Heb je al account? <a href="{{route('login')}}">Ga naar de login</a></p>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script>
    $("input:checkbox").on('click', function() {
        // in the handler, 'this' refers to the box clicked on
        var $box = $(this);
        if ($box.is(":checked")) {
            // the name of the box is retrieved using the .attr() method
            // as it is assumed and expected to be immutable
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            // the checked state of the group/box on the other hand will change
            // and the current value is retrieved using .prop() method
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", false);
        }
    });
</script>


@endsection
