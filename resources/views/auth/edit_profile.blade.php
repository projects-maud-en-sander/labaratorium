@extends('layouts.app')

@section('head')
    <link href="{{ asset('css/profile.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container mt-2">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Profiel aanpassen</div>

                    <div class="card-body">
                        <form action="{{ route('profile.update', $profile) }}" method="POST">
                            @method('PATCH')
                            @csrf

                            <label for="name">Naam</label>
                            <input class="form-control {{ $errors->has('name') ? ' has-error' : '' }}" type="text" name="name" id="name"
                                   value="{{ $profile->name}}"><small class="text-danger">{{ $errors->first('name') }}</small><br>

                            <label for="email">Email</label>
                            <input class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" type="text" name="email" id="email"
                                      value="{{ $profile->email}}"><small class="text-danger">{{ $errors->first('email') }}</small>

                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Ander wachtwoord kiezen?') }}
                                </a>
                            @endif

                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary">Opslaan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
