@extends('layouts.app')

@section('head')
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="card" style="border-color: #FFC929; border-width: 1px; padding-bottom: 50px">
                    <div class="card-body">
                        <div class="text-center" style="color: #FFC929; font-size: 45px; font-weight: bold; font-family: 'Segoe UI'">Login</div>
                        <br>

                    <!-- This is the login form you have to enter a email and a password here if invalid the form will show error messages -->
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row pb-0 mb-0">
                            <label for="email" class="col col-form-label" style="font-weight: bold">{{ __('E-Mail') }}</label>
                        </div>

                        <div class="form-group row">
                            <div class="col">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row pb-0 mb-0">
                            <label for="password" class="col col-form-label" style="font-weight: bold">{{ __('Wachtwoord') }}</label>
                        </div>
                            <div class="form-group row">
                            <div class="col">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <div class="col-6 offset-0">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Onthoud mij') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 ">
                                <button type="submit" class="btn rounded-0" style="background-color: #FFC929; color: white">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Wachtwoord vergeten?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                        <p class="mt-2">Heb je nog geen account? <a href="{{route('register')}}"> Maak een account aan.</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
