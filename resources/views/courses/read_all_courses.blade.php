@extends('layouts.app')

@section('head')
    <link href="{{ asset('css/courses.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <h2 class="ml-2">Kies de cursus die je wilt volgen</h2><br>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-11 dropdown m-1">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Filteren op:
                </button>


                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="{{route('courses.index', ['name' => request('name'), 'name' => 'asc']) }}"> Naam (A-Z)</a>
                    <a class="dropdown-item" href="{{route('courses.index', ['name' => request('name'), 'name' => 'desc']) }}">Naam (Z-A)</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{route('courses.index', ['level' => request('level'), 'level' => 'asc']) }}">level (↓-↑)</a>
                    <a class="dropdown-item" href="{{route('courses.index', ['level' => request('level'), 'level' => 'desc']) }}">level (↑-↓)</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{route('courses.index', ['description' => request('description'), 'description' => 'asc']) }}">Beschrijving (A-Z)</a>
                    <a class="dropdown-item" href="{{route('courses.index', ['description' => request('description'), 'description' => 'desc']) }}">Beschrijving (Z-A)</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{route('courses.index', ['created_at' => request('created_at'), 'created_at' => 'asc']) }}">Datum (↓-↑)</a>
                    <a class="dropdown-item" href="{{route('courses.index', ['created_at' => request('created_at'), 'created_at' => 'desc']) }}">Datum (↑-↓)</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{route('courses.index', ['status_id' => request('status_id'), 'status_id' => 'asc']) }}">status (↓-↑)</a>
                    <a class="dropdown-item" href="{{route('courses.index', ['status_id' => request('status_id'), 'status_id' => 'desc']) }}">status (↓-↑)</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/courses">Reset</a>
                </div>
            </div>
                @foreach($courses as $course)
                            <div class="mr-2 mb-2 col-lg-4 col-sm-12">
                                <div class="card rounded-0"
                                     style="border-color: #45B97C; border-width: 1px;">
                                    <a href="{{route('courses.show', $course)}}">
                                        <div class="card-body">
                                            <h5 class="card-title">{{$course->name}}</h5>
                                        </div>
                                    </a>

                                    @foreach($courses_status as $course_status)
                                        @if($course_status->courses_id == $course->id)
                                            @if($course_status->status_id == 2)
                                                <i class="fa fa-check align-self-end mr-3"  aria-hidden="true"></i>
                                            @endif
                                        @endif
                                    @endforeach

                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-sm-secondary" data-toggle="modal"
                                            data-target="#course_modal">
                                        Meer info
                                    </button>
                                </div>

                                <!-- Modal -->
                                <div class="modal" id="course_modal" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div>
                                            </div>
                                            <div class="modal-body">
                                                {{$course->description}}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                @endforeach



            <div class="container row justify-content-center">

            </div>
        </div>

    </div>


@endsection

