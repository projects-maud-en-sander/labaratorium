@extends('layouts.app')

@section('content')
    <div class="container m-2">
        <form action="{{route('admin.roles_update.index')}}" method="get">
            @csrf
            <select id="filter" name="sort" onchange="this.form.submit()">
                <option value="" disabled selected>Sorteer</option>
                <option value="name_asc">Naam ↑</option>
                <option value="name_desc">Naam ↓</option>
                <option value="email_asc">Email ↑</option>
                <option value="email_desc">Email ↓</option>
                <option value="student_number_asc">Student nummer ↑</option>
                <option value="student_number_desc">Student nummer ↓</option>
            </select>
        </form>
    </div>

    <div class="container-fluid">
        <div class="row ">
            <div class="d-sm-inline-flex mr-2 ml-4" style="overflow-x: auto">
                <!-- show the data of all the user in table -->
                <table id="dtHorizontalVerticalExample" class="table table-striped table-bordered table-sm "
                       cellspacing="0"
                       width="100%">
                    <thead>
                    <tr>
                        <th>Naam student</th>
                        <th>Email</th>
                        <th>Student nummer</th>
                        <th>Rol</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!Empty($users_role))
                        @foreach($users_role as $user_role)
                            <tr>
                                <td>{{$user_role['name']}}</td>
                                <td>{{$user_role['email']}}</td>
                                <td>{{$user_role['student_number']}}</td>
                                <td>{{$user_role['roles']['role']}}</td>
                                <td><a href="{{route('admin.roles_update.edit', $user_role['id'])}}" ><i class="fas fa-pen"></i></a></td>

                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


