@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="mb-3">Bootcamp Forums</h1>

                <div class="card">
                    <div class="card-header h5">Nieuwe post plaatsen</div>

                    <div class="card-body">
                        <form action="{{ route('admin.topics.posts.store', $topic) }}" method="POST">
                            @csrf

                            <p>Je post wordt in deze topic geplaatst:<br>
                                {{ $topic->name }}
                            </p>

                            <label for="title">Naam van je post</label>
                            <input class="form-control" type="text" name="title" id="title"> <br>

                            <label for="message">Berichttekst</label>
                            <textarea class="form-control" type="text" name="message"
                                      id="message" rows="4"></textarea> <br>

                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary">Opslaan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
