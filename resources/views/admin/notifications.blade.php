@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-md-12">
                <h1>Homepage Melding Beheer</h1>
                <form action="{{ route('admin.notifications.create') }}" method="GET" class="float-right">
                    <button type="submit" class="float-right btn btn-primary ">Voeg een nieuwe melding toe</button>
                </form>
            </div>
        </div>

        <div class="row">
            @foreach($notifications as $notification)
                <div class="col-xl-3 col-md-6 col-sm-11 d-flex mb-2">
                    <div class="card rounded-0"
                         style="border-color: #45B97C; border-width: 1px; width: 100%">
                        <div class="card-header bg-transparent">
                            <p style="color: #FFC929; font-family: 'Segoe UI', serif; font-weight: bold; ">
                                {{ $notification->title }}
                            </p>

                            <div class="row-cols-md-4 row-cols-sm-12">
                                <form action="{{ route('admin.notifications.edit', $notification) }}" method="GET"
                                      class="float-xl-right float-xl-right float-md-left float-sm-left mr-2 ">
                                    <button type="submit" class="btn btn-primary"><i
                                            class="fas fa-edit"></i>
                                    </button>
                                </form>

                                <form action="{{ route('admin.notifications.destroy', $notification)}}" method="POST"
                                      class="float-xl-right float-xl-right mr-2 float-md-left float-sm-left">
                                    @method('delete')
                                    @csrf

                                    <button type="submit" name="id" value="Delete" class="btn btn-danger">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div class="card-body d-flex flex-column">
                            <p>{!! $notification->message !!}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
@endsection
