@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="mb-3">Bootcamp Forums</h1>

                <div class="card">
                    <div class="card-header">
                        <h4 class="mt-1">Reageer op {{ $forum_post->title }}</h4>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('admin.topics.posts.replies.store', [$topic, $forum_post]) }}" method="POST">
                            @method('POST')
                            @csrf

                            <textarea class="form-control" type="text" name="message"
                                      id="message" required></textarea> <br>

                            <button type="submit" class="btn btn-primary">Plaats reactie</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
