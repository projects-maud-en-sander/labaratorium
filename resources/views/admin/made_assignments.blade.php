@extends('layouts.app')

@section('content')
    <div class="col-sm-11 dropdown mb-3 mt-3">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Filteren op:
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{route('admin.made_assignments.index', ['name' => request('name'), 'name' => 'asc']) }}"> Naam (A-Z)</a>
            <a class="dropdown-item" href="{{ route('admin.made_assignments.index', ['name' => request('name'), 'name' => 'desc']) }}">Naam (Z-A)</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{route('admin.made_assignments.index', ['course_name' => request('course_name'), 'course_name' => 'asc']) }}">Cursus (A-Z)</a>
            <a class="dropdown-item" href="{{route('admin.made_assignments.index', ['course_name' => request('course_name'), 'course_name' => 'desc']) }}">Cursus (A-Z)</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{route('admin.made_assignments.index', ['assignment_name' => request('assignment_name'), 'assignment_name' => 'asc']) }}">Opdracht (A-Z)</a>
            <a class="dropdown-item" href="{{route('admin.made_assignments.index', ['assignment_name' => request('assignment_name'), 'assignment_name' => 'desc']) }}">Opdracht (Z-A)</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{route('admin.made_assignments.index', ['updated_at' => request('updated_at'), 'updated_at' => 'asc']) }}">Datum (↓-↑)</a>
            <a class="dropdown-item" href="{{route('admin.made_assignments.index', ['updated_at' => request('updated_at'), 'updated_at' => 'desc']) }}">Datum (↑-↓)</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{route('admin.made_assignments.index')}}">Reset</a>
        </div>
    </div>

    <div class="container-fluid ml-4">
        <div class="row ">
            <div class="d-sm-inline-flex mr-2" >
                <table id="dtHorizontalVerticalExample" class="table table-striped table-bordered table-sm "
                       cellspacing="0"
                       width="100%">
                    <thead>
                    <tr>
                        <th>Naam student</th>
                        <th>Cursus</th>
                        <th>Opdracht</th>
                        <th>Datum</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!Empty($made_assignments))
                        @foreach($made_assignments as $made_assignment)
                            <tr>
                                <td>{{$made_assignment->name}}</td>
                                <td>{{$made_assignment->course_name}}</td>
                                <td>{{$made_assignment->assignment_name}}</td>
                                <td>{{$made_assignment->updated_at}}</td>
                                <td><a href="{{route('admin.made_assignments.show',  $made_assignment->assignment_id)}}" class="btn btn-info"><i class="far fa-eye"></i><i class="fas fa-pen"></i></a></td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


