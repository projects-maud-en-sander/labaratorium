@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Maak een nieuwe cursus aan </div>
                    <div class="card-body">

                        <form action="{{route('admin.courses.store')}}" method="POST">
                            @method('POST')
                            @csrf

                            <label for="name">Naam:</label>
                            <input class="form-control" type="text" name="name" id="name" required>

                            <label for="description">Beschrijving:</label>
                            <textarea class="form-control" type="text" name="description" id="description" required></textarea> <br>

                            <label for="level">Level:</label>
                            <input class="form-control" type="number" name="level" id="level" required>

                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary">Maak cursus aan</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
