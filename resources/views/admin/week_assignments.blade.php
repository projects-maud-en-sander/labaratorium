@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Week opdrachten</div>

                    <div class="card-body">
                        <a href="{{ route('admin.courses.assignments.week-assignments.create', [$course, $assignment]) }}"
                           class="btn btn-primary">Niewe weekopdracht aanmaken</a>

                        @if(!Empty($week_assignments))
                            @foreach($week_assignments as $week_assignment)
                                <a href="#">
                                    <div class="card-body bg-light row">
                                        {{ $week_assignment->name }}
                                    </div>
                                </a>
                                <form
                                    action={{ route('admin.courses.assignments.week-assignments.destroy', [$course, $assignment, $week_assignment])}} method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" name="id" value="Delete" class="btn btn-danger">
                                        Verwijder week opdracht
                                    </button>
                                    <a href="{{route('admin.courses.assignments.week-assignments.edit', [$course, $assignment, $week_assignment])}}"
                                       class="btn btn-primary">Wijzig</a>
                                </form>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
