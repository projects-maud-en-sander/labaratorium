@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="mb-3">Bootcamp Forums</h1>

                <div class="card">
                    <div class="card-header h5">Post aanpassen</div>

                    <div class="card-body">
                        <form action="{{ route('admin.topics.posts.update', [$topic, $forum_post])}}" method="POST">
                            @method('PATCH')
                            @csrf

                            <label for="title">Naam van de post</label>
                            <input class="form-control" type="text" name="title" id="title"
                                   value="{{ $forum_post->title}}"> <br>

                            <label for="message">Berichttekst</label>
                            <textarea class="form-control" type="text" name="message"
                                      id="message" rows="4">{{ $forum_post->message}}</textarea> <br>

                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary">Opslaan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
