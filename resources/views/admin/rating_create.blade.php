@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a href="{{route('admin.download-file', $made_assignment_files->assignment_location)}}">Download</a>
                <div class="card">
                    <div class="card-header">Beoordelen</div>
                    <div class="card-body">
                        <!-- Form for creating a assignment. You have to enter a name,
                             the course is already filled in and hidden -->
                        <form action="{{ route('admin.made_assignments.update', $made_assignment_files) }}" method="POST">
                            @method('PATCH')
                            @csrf
                            <label for="rating">Cijfer:</label>
                            <input value="@if(!empty($made_assignment_files->rating)){{$made_assignment_files->rating}}@endif" class="form-control @error('rating') is-invalid @enderror" type="text" name="rating" id="rating" required><br>
                            <label for="feedback">Feedback:</label>
                            <textarea id="feedback" class="form-control @error('feedback') is-invalid @enderror">
                                @if(!empty($made_assignment_files->feedback)){{$made_assignment_files->feedback}}@endif
                            </textarea>
                            @if($assignment->final_assignment == 1)
                                <input type="checkbox" name="level" value="1"><div>Volgend niveau</div>
                            @endif
                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary">Opslaan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
