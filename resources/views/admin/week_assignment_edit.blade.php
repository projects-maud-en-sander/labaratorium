@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Opdracht aanpassen</div>
                    <div class="card-body">
                        <form action="{{ route('admin.courses.assignments.week-assignments.update', [$course, $assignment, $week_assignment]) }}" method="POST" enctype="multipart/form-data">
                            @method('PATCH')
                            @csrf

                            <label for="name">Naam Opdracht:</label>
                            <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" id="name"
                                   value="{{ $week_assignment->name }}"> <br>

                            <label for="name">Naam Opdracht:</label>
                            <input class="form-control @error('file') is-invalid @enderror" type="file" name="file" id="file"
                                   value="{{ $week_assignment->file_location }}"> <br>

                            <label for="name">Naam Opdracht:</label>
                            <input class="form-control @error('video') is-invalid @enderror" type="file" name="video" id="video"
                                   value="{{ $week_assignment->file_location }}"> <br>

                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary">Opslaan</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
