@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header h5">Melding aanpassen</div>

                    <div class="card-body">
                        <form action="{{ route('admin.notifications.update', $notification) }}" method="POST">
                            @csrf
                            @method('PATCH')

                            <label for="title">Titel van de melding (maak deze niet te lang)</label>
                            <input class="form-control" type="text" name="title" id="title"
                                   value="{{ $notification->title }}"> <br>

                            <label for="message">Meldingstekst</label>
                            <textarea class="form-control" type="text" name="message"
                                      id="message" rows="4">{{ $notification->message }}</textarea> <br>

                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary">Opslaan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
