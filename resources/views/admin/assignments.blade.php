@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card-body">
                    @foreach($assignments as $assignment)
                        <a href="{{route('admin.courses.assignments.show', [$course, $assignment])}}">
                            <div class="card-body bg-light row">
                                {{ $assignment->name }} - {{ $assignment->courses_id }}
                            </div>
                        </a>
                        <form
                            action={{ route('admin.courses.assignments.destroy', [$course, $assignment])}} method="POST">
                            @method('delete')
                            @csrf
                            <button type="submit" name="id" value="Delete" class="btn btn-danger">
                                Verwijder opdracht
                            </button>
                            <a href="{{route('admin.courses.assignments.edit', [$course, $assignment])}}"
                               class="btn btn-primary">Wijzig</a>
                        </form>
                    @endforeach
                </div>
                <div class="row justify-content-center offset-5 mt-2">
                    <a href="{{route('admin.courses.assignments.create', $course)}}" class="btn btn-primary">Opdracht
                        toevoegen</a>
                </div>
            </div>
        </div>
    </div>
@endsection
