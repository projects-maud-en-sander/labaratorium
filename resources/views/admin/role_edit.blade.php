@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="ml-4">
            <div class="row">
                <div class="col-12">
                    <!-- show al the user data as it is now -->
                    <div class="card-body">
                        Naam: {!! $user->name !!}<br>
                        Email: {!! $user->email !!}<br>
                        Student nummer: {!! $user->student_number !!}<br>
                        Rol: {!! $role->role !!}
                    </div>
                </div>
            </div>
            <div class="row ">
                <!-- form to change the role of the user -->
                <form action="{{ route('admin.roles_update.update', $user) }}" method="POST">
                    @method('PATCH')
                    @csrf
                    <label>
                        <select class="mr-4 form-control" name="role" id="role">
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{!! $role->role !!}</option>
                            @endforeach
                        </select>
                    </label>
                    <button type="submit" class="form-control">Wijzig</button>
                </form>
                <!-- form -->
            </div>
        </div>
    </div>
@endsection


