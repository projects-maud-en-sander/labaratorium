@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Opdracht Aanmaken</div>
                    <div class="card-body">
                        <!-- Form for creating a assignment. You have to enter a name,
                             the course is already filled in and hidden -->
                        <form action="{{ route('admin.courses.assignments.store', $course) }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <label for="name">Naam:</label>
                            <input class="form-control @error('name') is-invalid @enderror" type="text" name="name"
                                   id="name" required><br>

                            <label for="description">Beschrijving:</label>
                            <textarea class="form-control" type="text" name="description" id="description"
                                      required></textarea> <br>

                            <label for="name">Upload bestand:</label>
                            <input class="form-control @error('file') is-invalid @enderror" type="file" name="file"
                                   id="file"> <br>

                            <label for="name">Upload video:</label>
                            <input class="form-control @error('video') is-invalid @enderror" type="file" name="video"
                                   id="video"> <br>

                            <div class="mt-2">
                                <div class="row">
                                    <div class="custom-control custom-checkbox col ml-3">
                                        <input type="hidden" name="end_assignment_check" value="0">

                                        <input type="checkbox"
                                               class="custom-control-input" id="defaultUnchecked1"
                                               name="end_assignment_check" value="1">

                                        <label class="custom-control-label" for="defaultUnchecked1">Eindopdracht</label>
                                    </div>
                                </div>

                                <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Opslaan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
