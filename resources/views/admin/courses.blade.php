@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-md-12">
                <h1>Cursus Beheer</h1>
                <form action="{{ route('admin.courses.create') }}" method="GET" class="float-right">
                    <button type="submit" class="float-right btn btn-primary ">Voeg een nieuwe cursus toe</button>
                </form>
            </div>
        </div>
        <div class="container m-2">
            <div class="row">
                <div class="col-md-2">
                    <form action="{{route('admin.courses.index')}}" method="get">
                        {{--                    @method('PATCH')--}}
                        @csrf
                        <select class="form-control form-control-sm" id="filter" name="sort"
                                onchange="this.form.submit()">
                            <option value="" disabled selected>Sorteer</option>
                            <option value="name_asc">Naam ↑</option>
                            <option value="name_desc">Naam ↓</option>
                            <option value="level_asc">Level ↑</option>
                            <option value="level_desc">Level ↓</option>
                            <option value="date_asc">Aangemaakt ↑</option>
                            <option value="date_desc">Aangemaakt ↓</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($courses as $course)
                <div class="col-xl-3 col-md-6 col-sm-11 d-flex mb-2">
                    <div class="card rounded-0"
                         style="border-color: #45B97C; border-width: 1px; width: 100%">
                        <div class="card-header bg-transparent">
                            <a href="{{route('admin.courses.assignments.index', $course)}}"
                               style="color: #FFC929; font-family: 'Segoe UI', serif; font-weight: bold; ">
                                {{$course->name}}
                            </a>
                            <div class="row-cols-md-4 row-cols-sm-12">
                                <form action="{{ route('admin.courses.edit', $course) }}" method="GET"
                                      class="float-xl-right float-xl-right float-md-left float-sm-left mr-2 ">
                                    <button type="submit" class="btn btn-primary"><i
                                            class="fas fa-edit"></i>
                                    </button>
                                </form>
                                <form action="{{ route('admin.courses.destroy', $course)}}" method="POST"
                                      class="float-xl-right float-xl-right mr-2 float-md-left float-sm-left">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" name="id" value="Delete" class="btn btn-danger">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div class="card-body d-flex flex-column">
                            <p>{!! $course->description !!}</p>
                            <p class="mt-auto">Niveau: {!! $course->level !!}</p>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
@endsection
