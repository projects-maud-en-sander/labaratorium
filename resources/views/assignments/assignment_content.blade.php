@extends('layouts.app')

@section('head')
    <link href="{{ asset('css/assignments.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container mt-2">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $assignment_contents->name }}</div>
                    <div class="card-body">
                        <div class="card-title">
                            {{ $assignment_contents->description }}
                        </div>
                        <div class="card-body bg-light row">
                            <a href="{{route('download-assignment-file',  $assignment_contents->file_location)}}"

                               target="_blank">
                                <button class="btn"><i
                                        class="fa fa-download"></i> {!! $assignment_contents->file_location !!}
                                </button>
                            </a>
                        </div>
                        <div class="wrapper mt-2">
                            <video preload controls style="width:400px;"
                                   poster="/public/images/bootstrap_tutorial.png">
                                <source src="/videos/{{ $assignment_contents->video_location }}"
                                        type="video/mp4">
                                Je browser support geen mp4 videos, verander van browser!
                            </video>
                        </div>
                    </div>
                </div>

                <div class="card mt-2 mb-4">
                    <div class="card-header">Opdracht Inleveren</div>
                    <div class="card-body">
                        @if(isset($succes))
                            <div class="alert alert-success">{{ $succes }}</div>
                        @endif
                        @if(!empty($user_assignments->assignment_location))
                            <div class="alert alert-success">
                                Deze opdracht is ingeleverd op {{$user_assignments->updated_at}}
                            </div>
                        @endif
                        <form action="{{ route('assignments.update', [$assignment_contents]) }}" method="POST"
                              enctype="multipart/form-data">
                            @method('PATCH')
                            @csrf

                            <label for="name"></label>
                            <input class="form-control @error('file') is-invalid @enderror" type="file" name="file"
                                   id="file"
                                   value="{{ $assignment_contents->assignment_location }}"> <br>
                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary">Opslaan</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
