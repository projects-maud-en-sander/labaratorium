@extends('layouts.app')

@section('content')
    <div class="col-sm-11 dropdown mb-3 mt-3">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Filteren op:
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{route('reviewed_assignments.index', ['course_name' => request('course_name'), 'course_name' => 'asc']) }}"> Cursus (A-Z)</a>
            <a class="dropdown-item" href="{{ route('reviewed_assignments.index', ['course_name' => request('course_name'), 'course_name' => 'desc']) }}">Cursus (Z-A)</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{route('reviewed_assignments.index', ['assignment_name' => request('assignment_name'), 'assignment_name' => 'asc']) }}">Opdracht (A-Z)</a>
            <a class="dropdown-item" href="{{route('reviewed_assignments.index', ['assignment_name' => request('assignment_name'), 'assignment_name' => 'desc']) }}">Opdracht (Z-A)</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{route('reviewed_assignments.index', ['rating' => request('rating'), 'rating' => 'asc']) }}">Cijfer (↓-↑)</a>
            <a class="dropdown-item" href="{{route('reviewed_assignments.index', ['rating' => request('rating'), 'rating' => 'desc']) }}">Cijfer (↑-↓)</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{route('reviewed_assignments.index', ['feedback' => request('feedback'), 'feedback' => 'asc']) }}">Feedback (A-Z)</a>
            <a class="dropdown-item" href="{{route('reviewed_assignments.index', ['feedback' => request('feedback'), 'feedback' => 'desc']) }}">Feedback (Z-A)</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{route('reviewed_assignments.index', ['updated_at' => request('updated_at'), 'updated_at' => 'asc']) }}">Datum (↓-↑)</a>
            <a class="dropdown-item" href="{{route('reviewed_assignments.index', ['updated_at' => request('updated_at'), 'updated_at' => 'desc']) }}">Datum (↑-↓)</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/reviewed_assignments">Reset</a>
        </div>
    </div>

    <div class="container-fluid ml-4">
        <div class="row">
            <div class="d-sm-inline-flex mr-2" >
                <table id="dtHorizontalVerticalExample" class="table table-striped table-bordered table-sm "
                       cellspacing="0"
                       width="100%">
                    <thead>
                    <tr>
                        <th>Cursus</th>
                        <th>Opdracht</th>
                        <th>Cijfer</th>
                        <th>Feedback</th>
                        <th>Datum</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(!Empty($reviewed_assignments))

                            @foreach($reviewed_assignments as $reviewed_assignment)
                                @if(!empty($reviewed_assignment->rating))
                                <tr>
                                    <td>{{$reviewed_assignment->course_name}}</td>
                                    <td>{{$reviewed_assignment->assignment_name}}</td>
                                    <td>{{$reviewed_assignment->rating}}</td>
                                    <td>{{$reviewed_assignment->feedback}}</td>
                                    <td>{{$reviewed_assignment->updated_at}}</td>
                                </tr>
                                @endif
                            @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
