@extends('layouts.app')

@section('head')
    <link href="{{ asset('css/assignments.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div>
                <h2>Opdrachten van {{ $courses->name }} </h2><br>
            </div>
            <div class="container m-2">
                <div class="row">
                    <div class="col-md-2">
                        <form action="{{route('courses.show', $courses)}}" method="get">
                            {{--                    @method('PATCH')--}}
                            @csrf
                            <select class="form-control btn-secondary" id="filter" name="sort" onchange="this.form.submit()">
                                <option value="" hidden selected>Sorteer</option>
                                <option value="name_asc">Naam ↑</option>
                                <option value="name_desc">Naam ↓</option>
                                <option value="date_asc">Aangemaakt ↑</option>
                                <option value="date_desc">Aangemaakt ↓</option>
                            </select>
                        </form>
                    </div>
                </div>
            </div>
            <div class="container d-flex flex-wrap row justify-content-center">
                @foreach($assignments as $assignment)
                    <div class="col-lg-3 col-md-4 col-sm-12 mb-3 d-flex align-items-stretch">
                        <a href="{{route('assignments.show', $assignment)}}">
                            <div class="card rounded-0"
                                 style="border-color: #45B97C; border-width: 1px; padding-bottom: 50px;">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $assignment->name }}</h5>

                                    @foreach($user_assignments as $user_assignment)
                                        @if($user_assignment->assignments_id == $assignment->id)
                                            @if($user_assignment->status_id == 2)
                                                <i class="fa fa-check align-self-end mr-3"  aria-hidden="true"></i>
                                            @endif
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
@endsection
