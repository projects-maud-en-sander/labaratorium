<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('images/b-ico.jpg') }}" type="image/x-icon" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/all.js') }}" defer></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

@yield('head')
<!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/nav.css') }}" rel="stylesheet">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <div id="wrapper" class="d-flex">
        @if(Auth::check())
            @include('layouts.partials.nav')
        @endif
        <div id="page-content-wrapper">
            <main class="mt-2">
                @if(Auth::check())
                    <button class="btn ml-2 mb-2" style="background-color: #FFC929;" id="menu-toggle">Menu</button>
{{--                        <a href="{{ url()->previous() }}" class="btn btn-outline-info mb-2"><i class="fa fa-angle-left"></i> Ga terug</a>--}}
                    <button class="btn btn-outline-info mb-2" onclick="goBack()">Ga terug</button>
                @endif
                @yield('content')
            </main>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>


<!-- Menu Toggle Script -->
<script type="text/javascript">
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

</script>
{{--Go back button script--}}
<script>
    function goBack() {
        window.history.back();
    }
</script>
@yield('script')

</body>
</html>
