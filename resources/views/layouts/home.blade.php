<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

@yield('head')
<!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-xl navbar-light shadow-sm" style="background-color: #FFC929">

        <a class="navbar-brand pb-3 pt-3" href="{{ url('home') }}">
            <img src="{{ asset('images/logo-bootcamp.png') }}">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="{{ Route::currentRouteNamed('courses.index') ? 'active' : '' }} nav-link text-white pb-3 pt-4"
                       style="font-weight: bold; font-size: 20px; font-family: 'Segoe UI'"
                       href="{{route('courses.index')}}">Cursussen</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white pb-3 pt-4"
                       style="font-weight: bold; font-size: 20px; font-family: 'Segoe UI'" href="{{route('forum')}}">Forums</a>
                </li>
                @if(!Empty(Auth::user()))
                    <li class="nav-item">
                        <a class="nav-link text-white pb-3 pt-4"
                           style="font-weight: bold; font-size: 20px; font-family: 'Segoe UI'"
                           href="{{url('/forum/my-posts')}}">Mijn posts</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link text-white pb-3 pt-4"
                           style="font-weight: bold; font-size: 20px; font-family: 'Segoe UI'" href="{{url('reviewed_assignments.index')}}">Beoordelingen</a>
                    </li>
                    @if(Auth::user()->roles_id == 3 || Auth::user()->roles_id == 2)
                    <!--  admin or teacher user navlinks -->
                        <li class="nav-item">
                            <a style="font-weight: bold; font-size: 20px; font-family: 'Segoe UI'"
                               href="{{route('admin.made_assignments.index')}}"
                               class="{{ Route::currentRouteNamed('admin.made_assignments.index') ? 'active' : '' }} nav-link text-white pb-3 pt-4">Gemaakte
                                opdrachten</a>
                        </li>

                        <li class="nav-item">
                            <a style="font-weight: bold; font-size: 20px; font-family: 'Segoe UI'"
                               href="{{route('admin.courses.create')}}"
                               class="{{ Route::currentRouteNamed('admin.courses.create') ? 'active' : '' }} nav-link text-white pb-3 pt-4">Cursus
                                aanmaken</a>
                        </li>
                        <!--  admin or teacher user navlinks -->
                    @endif
                <!-- if user is admin -->
                    @if(Auth::user()->roles_id == 3)
                    <!--  admin user navlinks -->
                        <li class="nav-item">
                            <a style="font-weight: bold; font-size: 20px; font-family: 'Segoe UI'"
                               href="{{route('admin.roles_update.index')}}"
                               class="{{ Route::currentRouteNamed('admin.roles_update.index') ? 'active' : '' }} nav-link text-white pb-3 pt-4">Verander
                                Rechten</a>
                        </li>
                        <!-- admin user navlinks -->
                    @endif
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav text-right">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link text-white pb-3 pt-4"
                           style="font-weight: bold; font-size: 20px; font-family: 'Segoe UI'"
                           href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link text-white pb-3 pt-4"
                               style="font-weight: bold; font-size: 20px; font-family: 'Segoe UI'"
                               href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre
                           style="font-weight: bold; font-size: 20px; font-family: 'Segoe UI'">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('profile.index') }}">
                                Profiel inzien
                            </a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </nav>

    <main class="py-0">
        @yield('content')
    </main>
</div>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</html>
