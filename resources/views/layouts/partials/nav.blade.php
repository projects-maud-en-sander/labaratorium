<!-- Sidebar -->
<div class="border-right" id="sidebar-wrapper">
    <div class="sidebar-heading">
        <a href="{{route('home')}}">
            <img src="{{ asset('images/sidenav-logo.png') }}">
        </a>
    </div>
    <div class="list-group list-group-flush">
        <a href="{{route('courses.index')}}"

           class="{{ Route::currentRouteNamed('courses.index') ? 'active' : '' }} nav-item nav-link">Cursussen</a>
        <a href="{{route('reviewed_assignments.index')}}" class="{{ Route::currentRouteNamed('reviewed_assignments.index') ? 'active' : '' }} nav-item nav-link">Beoordelingen</a>
        <a href="{{route('forum')}}" class="{{ Route::currentRouteNamed('forum') ? 'active' : '' }} nav-item nav-link">Forums</a>
        <a href="{{route('my-posts')}}" class="{{ Route::currentRouteNamed('my-posts') ? 'active' : '' }} nav-item nav-link">Mijn posts</a>
        <a href="{{route('profile.index') }}" class="{{ Route::currentRouteNamed('profile.index') ? 'active' : '' }} nav-item nav-link"> {{ Auth::user()->name }}</a>


        <!-- If user is teacher or admin -->
    @if(Auth::user()->roles_id == 3 || Auth::user()->roles_id == 2)
            <p class="text-muted border-bottom ml-1">Docent</p>
            <!--  admin or teacher user navlinks -->
            <a href="{{route('admin.courses.create')}}"
               class="{{ Route::currentRouteNamed('admin.courses.create') ? 'active' : '' }} nav-item nav-link">
                Nieuwe cursus aanmaken</a>
            <a href="{{route('admin.made_assignments.index')}}"
               class="{{ Route::currentRouteNamed('admin.made_assignments.index') ? 'active' : '' }} nav-item nav-link">
                Gemaakte opdrachten</a>
            <a href="{{route('admin.courses.index')}}"
               class="{{ Route::currentRouteNamed('admin.courses.index') ? 'active' : '' }} nav-item nav-link">
                Beheer Cursussen</a>
            <a href="{{route('admin.topics.index')}}"
               class="{{ Route::currentRouteNamed('admin.topics.index') ? 'active' : '' }} nav-item nav-link">
                Beheer Forums</a>
            <a href="{{route('admin.notifications.index')}}"
               class="{{ Route::currentRouteNamed('admin.notifications.index') ? 'active' : '' }} nav-item nav-link">
                Beheer Homepage Meldingen</a>

            <!--  admin or teacher user navlinks -->
    @endif

    <!-- if user is admin -->
    @if(Auth::user()->roles_id == 3)
            <p class="text-muted border-bottom ml-1">Admin</p>
            <!--  admin user navlinks -->
            <a href="{{route('admin.roles_update.index')}}"
               class="{{ Route::currentRouteNamed('admin.roles_update.index') ? 'active' : '' }} nav-item nav-link">
                Verander Gebruikersrechten</a>
            <!-- admin user navlinks -->
        @endif


        <a class="nav-item nav-link bottom-nav" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
            {{ __('Uitloggen') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>

</div>

<!-- /#sidebar-wrapper -->
