@extends('layouts.app')

@section('content')

    <div class="d-flex justify-content-center"><h1>Cursus aanpassen</h1></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card mb-4">

                        <div class="card-body">
                            <form action="{{ route('admin.courses.update', $course) }}" method="POST" enctype="multipart/form-data">
                                @method('PATCH')
                                @csrf

                                <label for="name">Naam:</label>
                                <input class="form-control" type="text" name="name" id="name"
                                       value="{{ $course->name }}" required> <br>

                                <label for="description">Beschrijving:</label>
                                <textarea rows="4" class="form-control" name="description" id="description" rows="6"
                                          required>{{ $course->description }}</textarea> <br>

                                <label for="level">Niveau:</label>
                                <input class="form-control" type="number" name="level" id="level"
                                       value="{{ $course->level }}" required>

                                <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Opslaan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
