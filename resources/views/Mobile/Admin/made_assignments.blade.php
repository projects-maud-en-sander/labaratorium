@extends('layouts.app')

@section('content')
    <div class="">
        <div class="row ">
            <table id="dtHorizontalVerticalExample" class="table table-striped table-bordered table-responsive-sm m-2"
                   cellspacing="0"
                   width="100%">
                <thead>
                <tr>
                    <th>Naam student</th>
                    <th>Cursus</th>
                    <th>Opdracht</th>
                    <th>Datum</th>
                </tr>
                </thead>
                <tbody>
                @if(!Empty($made_assignments))
                    @foreach($made_assignments as $made_assignment)
                        <tr>
                            <td>{{$made_assignment->name}}</td>
                            <td>{{$made_assignment->course_name}}</td>
                            <td>{{$made_assignment->assignment_name}}</td>
                            <td>{{$made_assignment->updated_at}}</td>
                            <td><a href="{{route('admin.made_assignments.show',  $made_assignment->assignment_id)}}"
                                   class="btn btn-info"><i class="far fa-eye"></i><i class="fas fa-pen"></i></a></td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection


