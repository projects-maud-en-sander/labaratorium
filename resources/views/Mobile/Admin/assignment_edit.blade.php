@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mb-4">
                    <div class="card-header">Opdracht aanpassen</div>

                    <div class="card-body">
                        <form action="{{ route('admin.courses.assignments.update', [$course, $assignment]) }}" method="POST" enctype="multipart/form-data">
                            @method('PATCH')
                            @csrf

                            <label for="name">Naam:</label>
                            <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" id="name"
                                   value="{{ $assignment->name }}"> <br>

                            <label for="description">Beschrijving:</label>
                            <textarea rows="4" class="form-control" type="text" name="description" id="description"
                                      required>{{ $assignment->description }}</textarea> <br>

                            <label for="name">Upload bestand:</label>
                            <input class="form-control @error('file') is-invalid @enderror" type="file" name="file" id="file"
                                   value="{{ $assignment->file_location }}"> <br>

                            <label for="name">Upload video:</label>
                            <input class="form-control @error('video') is-invalid @enderror" type="file" name="video" id="video"
                                   value="{{ $assignment->video_location }}"> <br>
                            <div class="mt-2">
                                <div class="row">
                                    <div class="custom-control custom-checkbox col">
                                        <input type="checkbox"
                                               class="custom-control-input @error('end_assignment_id') is-invalid @enderror"
                                               id="defaultUnchecked1" name="end_assignment_id" value="1">
                                        <label class="custom-control-label" for="defaultUnchecked1">Eind
                                            opdracht</label>
                                    </div>
                                </div>

                                <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Opslaan</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
