@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="h1 mb-3">Bootcamp Forums</div>

                <div class="h5 mt-2">
                    <a href="{{ route('topics.index') }}">Forums</a>
                    @foreach($path as $link)
                        > <a href="{{ route('topics.show', $link) }}">{{ $link->name }}</a>
                    @endforeach
                    > {{ $forum_post->title }}
                </div>

                <div class="card mb-3">
                    <div class="card-header border-bottom-0">
                        <div class="h4 mb-0">{{ $forum_post->title }}</div>
                    </div>

                    <div class="card-body border-top">
                        <div>{{ $forum_post->message }}</div> <br>

                        <div class="text-black-50">
                            Gepost door: {{ $forum_post->users->name }} <br>
                            Gepost op: {{ $forum_post->created_at }}

                            @if($forum_post->users_id == $user_id)
                                <div class="row mt-2">

                                    <a href="{{ route('topics.posts.edit', [$topic, $forum_post]) }}"
                                       class="btn btn-primary ml-3">Bewerk post</a>

                                    <form action="{{ route('topics.posts.destroy', [$topic, $forum_post])}}"
                                          method="POST">
                                        @method('delete')
                                        @csrf

                                        <button type="submit" class="btn btn-danger ml-2">
                                            Verwijder post
                                        </button>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="card-body border-top bg-light">
                        <a href="{{ route('topics.posts.replies.create', [$topic, $forum_post]) }}"
                           class="btn btn-primary mb-2">Reageer op dit bericht</a>

                        <div class="h5 mb-0">Reacties:</div>

                        <div class="mt-3">
                            <form action="{{route('topics.posts.show', [$topic, $forum_post])}}" method="get">
                                <select id="filter" name="sort" onchange="this.form.submit()">
                                    <option value="" disabled selected>Sorteer</option>
                                    <option value="message_asc">Bericht ↑</option>
                                    <option value="message_desc">Bericht ↓</option>
                                    <option value="name_asc">Naam ↑</option>
                                    <option value="name_desc">Naam ↓</option>
                                    <option value="created_at_asc">Datum ↑</option>
                                    <option value="created_at_desc">Datum ↓</option>
                                </select>
                            </form>
                        </div>
                    </div>

                    @foreach($replies as $reply)
                        <div class="card-body border-top">
                            <div>{{ $reply->message }}</div>
                            <br>
                            <div class="text-black-50 mb-1">
                                Gepost door: {{ $reply->users->name }} <br>
                                Gepost op: {{ $reply->created_at }}
                            </div>

                            @if($reply->users_id == $user_id)
                                <div class="row mt-2">
                                    <a href="{{ route('topics.posts.replies.edit', [$topic, $forum_post, $reply]) }}"
                                       class="btn btn-primary ml-3">Bewerk reactie</a>

                                    <form action="{{ route('topics.posts.replies.destroy', [$topic, $forum_post, $reply])}}"
                                        method="POST">
                                        @method('delete')
                                        @csrf

                                        <button type="submit" class="btn btn-danger ml-2">
                                            Verwijder reactie
                                        </button>
                                    </form>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
