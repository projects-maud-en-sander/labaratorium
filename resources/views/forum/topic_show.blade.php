@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="h1 mb-3">Bootcamp Forums</div>

                <div class="h5 mt-2">
                    <a href="{{ route('topics.index') }}">Forums</a>
                    @foreach($path as $link)
                        > <a href="{{ route('topics.show', $link) }}">{{ $link->name }}</a>
                    @endforeach
                    > {{ $show_topic->name }}
                </div>

                @if($topics->isEmpty() == false)
                    <div class="card mb-3">
                        <div class="card-header border-bottom-0">
                            <div class="h4 mb-0">Onderwerpen</div>

                            <div class="mt-3">
                                <form action="{{route('topics.show', $show_topic)}}" method="get">
                                    <select id="filter" name="sort_topics" onchange="this.form.submit()">
                                        <option value="" disabled selected>Sorteer</option>
                                        <option value="name_asc">Naam ↑</option>
                                        <option value="name_desc">Naam ↓</option>
                                        <option value="created_at_asc">Datum ↑</option>
                                        <option value="created_at_desc">Datum ↓</option>
                                    </select>
                                </form>
                            </div>
                        </div>

                        @foreach($topics as $topic)
                            <a class="h6 mb-0" href="{{ route('topics.show', $topic) }}">
                                <div class="card-body border-top">
                                    {{ $topic->name }}
                                </div>
                            </a>
                        @endforeach
                    </div>
                @endif

                @if($forum_posts->isEmpty() == false)
                    <div class="card mb-3">
                        <div class="card-header border-bottom-0">
                            <div class="h4 mb-0">Posts</div>

                            <a href="{{ route('topics.posts.create', $show_topic) }}"
                               class="btn btn-primary mt-2">Maak een nieuwe post</a>

                            <div class="mt-3">
                                <form action="{{route('topics.show', $show_topic)}}" method="get">
                                    <select id="filter" name="sort_posts" onchange="this.form.submit()">
                                        <option value="" disabled selected>Sorteer</option>
                                        <option value="title_asc">Titel ↑</option>
                                        <option value="title_desc">Titel ↓</option>
                                        <option value="created_at_asc">Datum ↑</option>
                                        <option value="created_at_desc">Datum ↓</option>
                                    </select>
                                </form>
                            </div>
                        </div>

                        @foreach($forum_posts as $forum_post)
                            <a class="h6 mb-0" href="{{ route('topics.posts.show', [$show_topic, $forum_post]) }}">
                                <div class="card-body border-top">
                                    {{ $forum_post->title }}
                                </div>
                            </a>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
