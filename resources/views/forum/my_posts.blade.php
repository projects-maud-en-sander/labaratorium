@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mt-1">
                    @if($my_posts->isEmpty() == false)
                        <div class="card-header border-bottom-0">
                            <h4 class="mt-1">Mijn Posts</h4>
                        </div>

                        @foreach($my_posts as $post)
                            <a href="{{ route('topics.posts.show', [$post->topics_id, $post]) }}">
                                <div class="card-body border-top">
                                    {{ $post->title }}
                                </div>
                            </a>
                        @endforeach
                    @else
                        <div class="card-header border-bottom-0">
                            <h4 class="mt-1">Je hebt nog geen posts gemaakt</h4>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
