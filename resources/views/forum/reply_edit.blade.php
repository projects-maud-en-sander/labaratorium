@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="mb-3">Bootcamp Forums</h1>

                <div class="card">
                    <div class="card-header">
                        <h4 class="mt-1">Bewerk je reactie op {{ $forum_post->title }}</h4>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('topics.posts.replies.update', [$topic, $forum_post, $reply]) }}" method="POST">
                            @method('PATCH')
                            @csrf

                            <textarea class="form-control" type="text" name="message"
                                      id="message" required>{{ $reply->message }}</textarea> <br>

                            <button type="submit" class="btn btn-primary">Bewerk reactie</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
