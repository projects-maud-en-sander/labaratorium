@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="h1 mb-3">Bootcamp Forums</div>

                <div class="h5 mt-2">
                    Forums
                </div>

                <div class="card mb-3">
                    <div class="card-header border-bottom-0">
                        <div class="h4 mb-0">Onderwerpen</div>

                        <div class="mt-3">
                            <form action="{{route('topics.index')}}" method="get">
                                <select id="filter" name="sort_topics" onchange="this.form.submit()">
                                    <option value="" disabled selected>Sorteer</option>
                                    <option value="name_asc">Naam ↑</option>
                                    <option value="name_desc">Naam ↓</option>
                                    <option value="created_at_asc">Datum ↑</option>
                                    <option value="created_at_desc">Datum ↓</option>
                                </select>
                            </form>
                        </div>
                    </div>

                    @foreach($topics as $topic)
                        <a class="h6 mb-0" href="{{ route('topics.show', $topic) }}">
                            <div class="card-body border-top">
                                {{ $topic->name }}
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
