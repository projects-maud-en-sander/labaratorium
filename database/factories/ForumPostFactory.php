<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Models\Topic;
use App\Models\ForumPost;
use Faker\Generator as Faker;

$factory->define(ForumPost::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'message' => $faker->text,
        'users_id' => User::all()->random()->id,
        'topics_id' => Topic::all()->random()->id,
    ];
});
