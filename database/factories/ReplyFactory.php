<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Models\ForumPost;
use App\Models\Reply;
use Faker\Generator as Faker;

$factory->define(Reply::class, function (Faker $faker) {
    return [
        'message' => $faker->text,
        'users_id' => User::all()->random()->id,
        'forum_posts_id' => ForumPost::all()->random()->id,
    ];
});
