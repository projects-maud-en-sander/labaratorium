<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

//The factory for the Assignment seeder that shows what needs to be created running the seeder
$factory->define(\App\Models\Assignment::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'courses_id' => $faker->numberBetween( 1, 10),
        'file_location' => 'Test_doc.txt',
        'video_location' => 'Eurovision2020_intro.mp4',
        'description' => $faker->text,
    ];
});
