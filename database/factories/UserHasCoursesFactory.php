<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models;
use App\Models\UserHasCourses;
use App\User;
use Faker\Generator as Faker;

$factory->define(UserHasCourses::class, function (Faker $faker) {
    return [
        'courses_id' => Models\Course::all()->random()->id,
        'users_id' => User::all()->random()->id,
        'status_id' => Models\Status::all()->random()->id,
    ];
});
