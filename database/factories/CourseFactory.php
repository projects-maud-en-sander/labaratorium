<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text,
        'level' => $faker->numberBetween( 1, 4),
        'topics_id' => \App\Models\Topic::all()->random()->id,
    ];
});
