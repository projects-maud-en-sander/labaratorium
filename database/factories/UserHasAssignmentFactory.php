<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models;
use App\Models\UserHasAssignment;
use App\User;
use Faker\Generator as Faker;

$factory->define(UserHasAssignment::class, function (Faker $faker) {
    return [
        'users_id' => User::all()->random()->id,
        'assignments_id' => Models\Assignment::all()->random()->id,
        'status_id' => Models\Status::all()->random()->id,
        'assignment_location' => 'teams.PNG',

    ];
});
