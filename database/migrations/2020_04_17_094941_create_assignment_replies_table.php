<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_replies', function (Blueprint $table) {
            $table->id();
            $table->text('message');
            $table->timestamps();
            $table->bigInteger('assignments_id')->unsigned()->index();
            $table->foreign('assignments_id')
                ->references('id')
                ->on('assignments')
                ->onDelete('cascade');
            $table->bigInteger('users_id')->unsigned()->index();
            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('week_assignment_replies');
    }
}
