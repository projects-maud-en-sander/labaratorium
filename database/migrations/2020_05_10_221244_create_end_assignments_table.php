<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEndAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('end_assignments', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('assignments_id')->unsigned()->index();
            $table->foreign('assignments_id')
            ->references('id')
            ->on('assignments')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('end_assignments');
    }
}
