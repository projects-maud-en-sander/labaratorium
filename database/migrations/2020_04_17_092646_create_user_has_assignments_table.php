<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class  CreateUserHasAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_has_assignments', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('users_id')->unsigned()->index();
            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->bigInteger('assignments_id')->unsigned()->index();
            $table->foreign('assignments_id')
                ->references('id')
                ->on('assignments')
                ->onDelete('cascade');
            $table->bigInteger('status_id')->unsigned()->index();
            $table->foreign('status_id')
                ->references('id')
                ->on('status')
                ->onDelete('cascade');
            $table->decimal('rating')->nullable();
            $table->text('feedback')->nullable();
            $table->string('assignment_location')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_has_assignments');
    }
}
