<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('replies', function (Blueprint $table) {
            $table->id();
            $table->text('message');
            $table->timestamps();
            $table->bigInteger('forum_posts_id')->unsigned()->index();
            $table->foreign('forum_posts_id')
                ->references('id')
                ->on('forum_posts')
                ->onDelete('cascade');
            $table->bigInteger('users_id')->unsigned()->index();
            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('replies');
    }
}
