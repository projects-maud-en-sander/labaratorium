<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('file_location');
            $table->string('video_location');
            $table->text('description');
            $table->integer('final_assignment')->nullable();
            $table->bigInteger('courses_id')->unsigned()->index();
            $table->foreign('courses_id')
                ->references('id')
                ->on('courses')
                ->onDelete('cascade');
            $table->bigInteger('topics_id')->unsigned()->index()->nullable();
            $table->foreign('topics_id')
                ->references('id')
                ->on('topics')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
