<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserHasCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_has_courses', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('users_id')->unsigned()->index();
            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->bigInteger('courses_id')->unsigned()->index();
            $table->foreign('courses_id')
                ->references('id')
                ->on('courses')
                ->onDelete('cascade');
            $table->bigInteger('status_id')->unsigned()->index();
            $table->foreign('status_id')
                ->references('id')
                ->on('status')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_has_courses');
    }
}
