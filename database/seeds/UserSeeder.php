<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class, 10)->create();

        DB::table('users')->insert([

            [
                'name' => 'Maud de Haas',
                'student_number' => 's1146976',
                'email' => 's1146976@student.windesheim.nl',
                'roles_id' => '3',
                'password' => '$2y$10$LegzsUIjwDoZTv1eh0IT8eFfMl3aUyV3R1CGP.olIhfK9KmJX6Jv6',
                'level' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]

        ]);

        DB::table('users')->insert([

            [
                'name' => 'Stefan Koelewijn',
                'student_number' => 's1145109',
                'email' => 's1145109@student.windesheim.nl',
                'roles_id' => '3',
                'password' => '$2y$10$zKtTKGPFERRTphMLr9tFD.mueesUtbzrub5U.gNimIkaTyQrY7sv.',
                'level' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]

        ]);

        DB::table('users')->insert([

            [
                'name' => 'Harm Hendrik ten Napel',
                'student_number' => 's1070857',
                'email' => 's1070857@student.windesheim.nl',
                'roles_id' => '3',
                'password' => '$2y$10$cGFQ4H8pGEfNZeGJaxZn/.JWCJ.5Neqz5YDuJ4w9NlexaDRbQdG1y',
                'level' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]

        ]);

        DB::table('users')->insert([

            [
                'name' => 'Sander',
                'student_number' => 's1141493',
                'email' => 's1141493@student.windesheim.nl',
                'roles_id' => '3',
                'password' => '$2y$10$e9Q8Y0BXuv9/sxWY9yVFeeomCmPtYhEL4WvsVLjvQqIZ4n9lIa.WC',
                'level' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]

        ]);
    }
}
