<?php

use Illuminate\Database\Seeder;

class UserHasCoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\UserHasCourses::class, 10)->create();
    }
}
