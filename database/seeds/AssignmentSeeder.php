<?php

use Illuminate\Database\Seeder;

class AssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Get the factory that is made for this seeder and run it X times
        factory(\App\Models\Assignment::class, 10)->create();
    }
}
