<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleSeeder::class,
            UserSeeder::class,
            TopicSeeder::class,
            ForumPostSeeder::class,
            ReplySeeder::class,
            StatusSeeder::class,
            CourseSeeder::class,
            AssignmentSeeder::class,
            UserHasAssignmentSeeder::class,
            NotificationSeeder::class,
            UserHasCoursesSeeder::class,
        ]);
    }
}
