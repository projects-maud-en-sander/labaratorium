<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('status')->get()->count() == 0) {

            DB::table('status')->insert([

                [
                    'id' => 1,
                    'name' => 'Doing',
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 2,
                    'role' => 'Done',
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 3,
                    'role' => 'Not started',
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")

                ]

            ]);

        } else {
            echo "\e[31mTable is not empty, therefore NOT ";
        }
    }
}
