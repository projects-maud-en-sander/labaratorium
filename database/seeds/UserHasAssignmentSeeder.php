<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserHasAssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\UserHasAssignment::class, 10)->create();
    }
}
