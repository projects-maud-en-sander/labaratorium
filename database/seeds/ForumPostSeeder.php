<?php

use Illuminate\Database\Seeder;
use App\Models\ForumPost;

class ForumPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ForumPost::class, 90)->create();
    }
}
