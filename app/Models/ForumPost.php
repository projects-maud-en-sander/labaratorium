<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ForumPost extends Model
{
    // Each forum post belongs to one topic.
    public function topics() {
        return $this->belongsTo(Topic::class);
    }

    // Each forum post belongs to one user.
    public function users() {
        return $this->belongsTo(User::class);
    }

    // Each forum post can have many replies.
    public function replies() {
        //Laravel automatically looks for forum_post_id, so we manually set the foreign key to forum_posts_id
        return $this->hasMany(Reply::class, 'forum_posts_id');
    }
}
