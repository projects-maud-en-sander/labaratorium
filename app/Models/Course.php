<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static find(int $id)
 */
class Course extends Model
{

    public static function join(string $string, \Closure $param)
    {
        //
    }

    // Each course can have many assignments.
    public function assignments()
    {
        //Automatically laravel looks for course_id, so we manually put it to the correct foreign_id from our table
        return $this->hasMany(Assignment::class, 'courses_id');
    }

    // Each course can have many users through user_has_courses.
    public function userhascourses()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign_id from our table
        return $this->hasMany(UserHasCourses::class, 'id');
    }

}
