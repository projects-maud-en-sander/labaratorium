<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // Each role can have many users.
    public function users()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign_id from our table
        return $this->hasMany(User::class);
    }
}
