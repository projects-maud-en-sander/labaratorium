<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    // Each profile has one user.
    public function user()
    {
        return $this->hasOne(User::class);
    }
}
