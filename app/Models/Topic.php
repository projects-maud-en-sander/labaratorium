<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    // Each topic belongs to one course or assignment.
    public function courses()
    {
        //Laravel automatically looks for topic_id, so we manually set the foreign key to topics_id
        return $this->belongsTo(Course::class, 'topics_id');
    }

    public function assignments()
    {
        //Laravel automatically looks for topic_id, so we manually set the foreign key to topics_id
        return $this->belongsTo(Assignment::class, 'topics_id');
    }

    // Each topic can have many forum posts.
    public function forum_posts() {
        //Laravel automatically looks for topic_id, so we manually set the foreign key to topics_id
        return $this->hasMany(ForumPost::class, 'topics_id');
    }
}
