<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static find(int $id)
 */
class UserHasCourses extends Model
{
    // Each user_has_assignment entry has one course.
    public function course()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign_id from our table
        return $this->hasMany(Course::class, 'id');
    }

    // Each user_has_assignment entry has one status.
    public function status()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign_id from our table
        return $this->hasOne(Status::class, 'id');
    }

    // Each user_has_assignment entry has one user.
    public function user()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign_id from our table
        return $this->hasOne(User::class, 'users_id');
    }

}
