<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    // Each assignment belongs to one course
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function users()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign_id from our table
        return $this->belongsToMany(User::class, 'user_has_assignments', 'assignments_id', 'assignments_id');
    }

    // Each assignment can have many users through user_has_assignments.
    public function user_has_assignment()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign_id from our table
        return $this->hasOne(UserHasAssignment::class, 'assignments_id');
    }

}
