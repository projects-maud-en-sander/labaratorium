<?php

namespace App;

use App\Models\Assignment;
use App\Models\ForumPost;
use App\Models\Reply;
use App\Models\Role;
use App\Models\UserHasCourses;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @method static find(int $int)
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'student_number', 'email', 'password', 'roles_id', 'level', 'status_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Each user can have many assignments through user_has_assignments.
    public function assignments()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign id from our table
        return $this->belongsToMany(Assignment::class, 'user_has_assignments', 'users_id', 'users_id');
    }

    // Each user can have many courses through user_has_assignments.
    public function userhascourses()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign id from our table
        return $this->belongsToMany(UserHasCourses::class, 'user_has_courses', 'users_id', 'users_id');
    }

    // Each user has one role.
    public function roles()
    {
        //Automatically laravel looks for role_id, so we manually put it to the correct foreign id from our table
        return $this->belongsTo(Role::class, 'roles_id');
    }

    // Each user can have many forum posts.
    public function forum_posts()
    {
        //Laravel automatically looks for user_id, so we manually set the foreign key to users_id
        return $this->hasMany(ForumPost::class, 'users_id');
    }

    // Each user can have many replies.
    public function replies()
    {
        //Laravel automatically looks for user_id, so we manually set the foreign key to users_id
        return $this->hasMany(Reply::class, 'users_id');
    }
}
