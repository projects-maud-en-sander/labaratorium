<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public $table = "status";

    // Each status belongs to many user_has_courses.
    public function userhascourses()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign id from our table
        return $this->belongsToMany(UserHasCourses::class, 'user_has_courses', 'status_id', 'status_id');
    }
}


