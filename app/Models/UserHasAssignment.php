<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserHasAssignment extends Model
{
    // Each user_has_assignment entry has one assignment.
    public function assignment()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign_id from our table
        return $this->hasOne(Assignment::class, 'id');
    }

    // Each user_has_assignment entry has one status.
    public function status()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign_id from our table
        return $this->hasOne(Status::class, 'id');
    }

    // Each user_has_assignment entry has one user.
    public function user()
    {
        //Automatically laravel looks for assignment_id, so we manually put it to the correct foreign_id from our table
        return $this->hasOne(User::class, 'users_id');
    }
}
