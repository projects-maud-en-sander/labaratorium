<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    // Each reply belongs to one forum post.
    public function forum_posts() {
        return $this->belongsTo(ForumPost::class);
    }

    // Each reply belongs to one user.
    public function users() {
        return $this->belongsTo(User::class);
    }
}
