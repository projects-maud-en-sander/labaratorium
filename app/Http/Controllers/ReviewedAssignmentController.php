<?php

namespace App\Http\Controllers;

use App\Models\Course;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;

class ReviewedAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //check witch column was requested and set it to column_name
        $queries = [];
        $columns = [
            'course_name', 'assignment_name', 'rating', 'feedback', 'updated_at'
        ];
        foreach ($columns as $column){
            if (request()->has($column)) {
                $column_name = $column;
                $queries[$column_name] = request($column);


            }
        }
        //if column_name is empty set column to course_name and queries to descending
        if (empty($column_name))
        {
            $column_name = 'course_name';
            $queries[$column_name] = 'desc';
        }

        //get the user id and then get al the data needed for the reviewed assignments with a join
        $user_id = Auth::id();
        $reviewed_assignments = DB::table('user_has_assignments')
            ->where('users_id', '=', $user_id)
            ->join('assignments', 'assignments.id', '=', 'user_has_assignments.assignments_id')
            ->join('courses', 'courses.id', '=', 'assignments.id')
            //users.name, assignments.name and courses.name get the same name in the result which makes them not visible changed name wwith as
            ->select('assignments.name as assignment_name', 'assignments.id as assignment_id', 'user_has_assignments.updated_at', 'courses.name as course_name', 'user_has_assignments.rating', 'user_has_assignments.feedback')
            ->orderBy($column_name , $queries[$column_name])
            ->get();

            //return the view with the reviewed assignments
            return view('assignments.reviewed_assignments_home', compact('reviewed_assignments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
