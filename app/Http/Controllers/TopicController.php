<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // Get the sorting keyword.
        $sort_topics = request('sort_topics');

        // Use the sorting keyword to retrieve and sort the topics.
        switch ($sort_topics) {
            case "name_asc":
                $topics = Topic::all()->where('topics_id', '=', null)->sortBy('name');
                break;
            case "name_desc":
                $topics = Topic::all()->where('topics_id', '=', null)->sortByDesc('name');
                break;
            case "created_at_asc":
                $topics = Topic::all()->where('topics_id', '=', null)->sortBy('creted_at');
                break;
            case "created_at_desc":
                $topics = Topic::all()->where('topics_id', '=', null)->sortByDesc('created_at');
                break;
            default:
                $topics = Topic::all()->where('topics_id', '=', null);
                break;
        }

        // Return the topic index view along with the topics.
        return view('forum.topics', compact('topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function show($topic)
    {
        // Retrieve the selected topic from the database.
        $show_topic = Topic::find($topic);

        // Get the topic sorting keyword.
        $sort_topics = request('sort_topics');

        // Use the topic sorting keyword to retrieve and sort the topics belonging to the current topic.
        switch ($sort_topics) {
            case "name_asc":
                $topics = Topic::all()->where('topics_id', '=', $topic)->sortBy('name');
                break;
            case "name_desc":
                $topics = Topic::all()->where('topics_id', '=', $topic)->sortByDesc('name');
                break;
            case "created_at_asc":
                $topics = Topic::all()->where('topics_id', '=', $topic)->sortBy('creted_at');
                break;
            case "created_at_desc":
                $topics = Topic::all()->where('topics_id', '=', $topic)->sortByDesc('created_at');
                break;
            default:
                $topics = Topic::all()->where('topics_id', '=', $topic);
                break;
        }

        // Get the forum post sorting keyword.
        $sort_posts = request('sort_posts');

        // Use the forum post sorting keyword to retrieve and sort the forum posts belonging to the current topic.
        switch ($sort_posts) {
            case "title_asc":
                $forum_posts = $show_topic->forum_posts->sortBy("title");
                break;
            case "title_desc":
                $forum_posts = $show_topic->forum_posts->sortByDesc("title");
                break;
            case "created_at_asc":
                $forum_posts = $show_topic->forum_posts->sortBy("created_at");
                break;
            case "created_at_desc":
                $forum_posts = $show_topic->forum_posts->sortByDesc("created_at");
                break;
            default:
                $forum_posts = $show_topic->forum_posts;
                break;
        }

        // Generate a "path" to the current topic.
        // This is done by getting the topic the current topic belongs to and then repeating this for
        // the topic you retrieved until you reach the base topic.
        // This is done so that you can easily navigate backwards.
        $path = [];
        $topic = Topic::find($topic);
        while ($topic->topics_id != null) {
            $topic = Topic::find($topic->topics_id);
            $path[] = $topic;
        }

        $path = collect($path)->reverse();

        // Return the topic show view along with:
        // - The current topic
        // - All topics the current topic contains
        // - All forum posts the current topic contains
        // - The path to the current topic
        return view('forum.topic_show', compact('show_topic', 'topics', 'forum_posts', 'path'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
