<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\UserHasAssignment;
use App\Models\UserHasCourses;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;


class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array|string
     */
    public function index()
    {
        // Load all the courses with the correct level and categories
        $courses = Course::where('level', '<=', Auth::user()->level);

        $queries = [];

        $columns = [
            'name', 'level', 'description', 'created_at'
        ];


        foreach ($columns as $column) {
            if (request()->has($column)) {
                $courses = $courses
                    ->orderBy($column, request($column));
                $queries[$column] = request($column);
            }
        }

        $courses = $courses->paginate(15)->appends($queries);

        if (request()->has('status_id')) {
            $status_id = request('status_id');

            switch ($status_id) {
                case "asc":
                    $user_has_courses = UserHasCourses::where('users_id', '=', Auth::user()->id)->paginate('15')->sortBy('status_id');

                    foreach ($user_has_courses as $uhc) {
                        $c[] = Course::find($uhc->courses_id);
                    }

                    $courses = collect($c);
                    break;
                case "desc":
                    $user_has_courses = UserHasCourses::where('users_id', '=', Auth::user()->id)->paginate('15')->sortByDesc('status_id');

                    foreach ($user_has_courses as $uhc) {
                        $c[] = Course::find($uhc->courses_id);
                    }

                    $courses = collect($c);
                    break;
                default:
                    $courses = Course::all()->where('level', '<=', Auth::user()->level);
                    break;
            }
        }


        //if the application is viewed on a mobile return a different view
        $agent = new Agent();

        $courses_status = UserHasCourses::where('users_id', '=', Auth::user()->id)->get();


        // Return the Courses view with the courses variable to use for looping through them
        return view('courses/read_all_courses', [
            'courses' => $courses,
            'columns' => $columns,
            'courses_status' => $courses_status,
        ]);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return string
     */
    public function show($id)
    {
        // Load all courses for the name at the top of the screen
        $courses = Course::find($id);
        //store the get from the form so i can use it to what everything needs to be filtered with
        $sort = request('sort');
        // if the $sort is in the request then use that to sort the output
        if ($sort == 'name_asc') {
            $assignment = Course::find($id)->assignments->sortBy('name');
        } elseif ($sort == 'name_desc') {
            $assignment = Course::find($id)->assignments->sortByDesc('name');
        } elseif ($sort == 'date_asc') {
            $assignment = Course::find($id)->assignments->sortBy('created_at');
        } elseif ($sort == 'date_desc') {
            $assignment = Course::find($id)->assignments->sortByDesc('created_at');
        } else {
            $assignment = Course::find($id)->assignments;
        }

        //Get the assignments the user made
        $user_assignments = UserHasAssignment::where('users_id', '=', Auth::id())->get();

        // This is how you return with a variable
        return view('assignments/assignments_home', [
            'assignments' => $assignment,
            'courses' => $courses,
            'user_assignments' => $user_assignments,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
