<?php

namespace App\Http\Controllers;

use App\Models\ForumPost;
use App\Models\Reply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create($topic, $forum_post)
    {
        // Retrieve the forum post the user wants to reply to from the database.
        $forum_post = ForumPost::find($forum_post);

        // Return the reply create view along with:
        // - The forum post the user wants to reply to
        // - The topic that forum post belongs to
        return view('forum.reply_create', compact('topic', 'forum_post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $topic, $forum_post)
    {
        // Validate the request.
        $request->validate( array(
            'message' => 'required|string',
        ));

        // Create and store a new reply.
        $reply = new Reply;

        $reply->message = $request->message;
        $reply->forum_posts_id = $forum_post;
        $reply->users_id = Auth::id();

        $reply->save();

        // Redirect to the show of the forum post the reply belongs to.
        return redirect()->route('topics.posts.show', [$topic, $forum_post]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $reply
     * @return \Illuminate\View\View | \Illuminate\Http\RedirectResponse
     */
    public function edit($topic, $forum_post, $reply)
    {
        // Retrieve the reply and the forum post it belongs to from the database.
        $forum_post = ForumPost::findOrFail($forum_post);
        $reply = Reply::findOrFail($reply);

        // If the reply belongs to the currently logged in user, return the reply edit view.
        if ($reply->users_id == Auth::user()->id) {
            return view('forum.reply_edit', compact('topic', 'forum_post', 'reply'));
        }

        // If not, redirect to the forum post show.
        return redirect()->route('topics.forum_posts.show', compact('topic', 'forum_post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $topic
     * @param  int  $forum_post
     * @param  int  $reply
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $topic, $forum_post, $reply)
    {
        // Validate the request.
        $request->validate( array(
            'message' => 'required|string',
        ));

        // Retrieve the reply from the database.
        $reply = Reply::findOrFail($reply);

        // If the reply belongs to the currently logged in user, update the reply.
        if (Auth::user()->id == $reply->users_id) {
            $reply->message = $request->message;

            $reply->update();
        }

        // Redirect back to the forum post the reply belongs to.
        return redirect()->route('topics.posts.show', [$topic, $forum_post]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($topic, $forum_post, $reply)
    {
        // Retrieve the reply from the database.
        $reply = Reply::findOrFail($reply);

        // If the reply belongs to the currently logged in user, delete the reply.
        if (Auth::user()->id == $reply->users_id) {
            $reply->delete();
        }

        // Redirect back to the forum post the reply belonged to.
        return redirect()->route('topics.posts.show', [$topic, $forum_post]);
    }
}
