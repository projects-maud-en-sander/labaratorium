<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;
use App\Models\Profile;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // Retrieve all users from the database.
        $profile = User::all();

        // Return the profile view.
        return view('auth.profile', [
            'profile' => $profile,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // Retrieve the user from the database.
        $profile = User::find($id);

        // Return the edit profile view.
        return view('auth/edit_profile', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProfileRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateProfileRequest $request, $id)
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|ends_with:windesheim.nl',
        ];

        $customMessages = [
            'name.required' => 'Vul je volledige naam in',
            'email.required' => 'Je email moet eindigen met @windesheim.nl',
        ];


        // validate if items from input are correct
        $data = $request->validated($request, $rules, $customMessages);

        // select the user
        $profile = User::find($id);

        // link the database data with the input data
        $profile->name = $data['name'];
        $profile->email = $data['email'];

        // execute the update
        $profile->save();

        // return to the profile index page
        return redirect()->route('profile.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }

}
