<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;

class AdminRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        //get all the user and their role
        $users_role = User::with('roles')->get();
        //put them in an easy readable array

        //store the get from the form so i can use it to what everything needs to be filtered with
        $sort = request('sort');
        if ($sort == 'name_asc'){
            $users_role = User::with('roles')->orderBy('name')->get();
        }elseif($sort == 'name_desc'){
            $users_role = User::with('roles')->orderByDesc('name')->get();
        }elseif($sort == 'email_asc'){
            $users_role = User::with('roles')->orderBy('email')->get();
        }elseif($sort == 'email_desc'){
            $users_role = User::with('roles')->orderByDesc('email')->get();
        }elseif($sort == 'student_number_asc'){
            $users_role = User::with('roles')->orderBy('student_number')->get();
        }elseif($sort == 'student_number_desc'){
            $users_role = User::with('roles')->orderByDesc('student_number')->get();
        }else{
            $users_role = User::with('roles')->get();
        }

        $users_role = $users_role->toArray();

        return view('admin.role_update', compact('users_role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        //get the role from this user
        $role = User::find($id)->roles;
        //gat the user
        $user = User::find($id);
        //get all roles
        $roles = Role::all();

        return view('admin.role_edit', compact('role', 'user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //see if the form was send with correct value
        $this->validate(request(),[
            'role' => 'required',
        ]);

        //find the user
        $user = User::find($id);
        //change the roles_id to the updated value
        $user->roles_id = $request->role;

        //update the user
        $user->update();

        return redirect()->route('admin.roles_update.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
