<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\UserHasAssignment;
use App\Models\UserHasCourses;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Assignment;
use Illuminate\Support\Facades\Response;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // load all courses
        $courses = Course::all();
        // load all the assignments
        $assignments = Assignment::all();

        $user_assignments = UserHasAssignment::where('users_id', '=', Auth::id())->get();
        //return the assignment view
        return view('assignments/assignments_home', [
            'assignments' => $assignments,
            'courses' => $courses,
            'user_assignments' => $user_assignments,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        // Select all content from the selected assignment
        $assignment_contents = Assignment::find($id);
        $user_id = auth()->id();

        //select the user assignment
        $user_assignments = DB::table('user_has_assignments')
            ->where('assignments_id', '=', $assignment_contents->id)
            ->where('users_id', '=', $user_id)
            ->first();

        // Return the view where it is displayed with variable from the select query
        return view('assignments/assignment_content', compact('assignment_contents', 'user_assignments'));

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $assignment_content)
    {
        //check if a file was uploaded and save is
        if ($request->file != Null) {
            $request->file->storeAs('files', $request->file->getClientOriginalName());
        }

        //check if a file was uploaded and save the location in user has assignments
        if ($request->file != Null){
            $user_id = auth()->id();

            $user_assignment = UserHasAssignment::where('assignments_id', '=', $assignment_content)
                ->where('users_id', '=', $user_id)
                ->first();

            //check if user_assignment is empty
            //if it is create a new user_has_assignment
            //if not update the assignment location
            if (!empty($user_assignment))
            {

                DB::table('user_has_assignments')
                    ->where('assignments_id', '=', $assignment_content)
                    ->where('users_id', '=', $user_id)
                    ->update(['assignment_location' => $request->file->getClientOriginalName()]);
                $succes = 'Het bestand is ingeleverd';
            }
            else
            {
               $user_assignment = new UserHasAssignment();
               $user_assignment->users_id = $user_id;
               $user_assignment->assignments_id = $assignment_content;
               $user_assignment->status_id = 1;
               $user_assignment->assignment_location = $request->file->getClientOriginalName();
               $user_assignment->save();
                $succes = 'Het bestand is ingeleverd';
            }
        }
        else {
            $succes = 'Er is een fout opgetreden, probeer het opnieuw';
        }


        //check if the user has this course
        $user_id = auth()->id();
        $course_id = Assignment::where('id', $assignment_content)->first('courses_id');
        $user_course = UserHasCourses::where('users_id', '=', $user_id)->where('courses_id', '=', $course_id->courses_id)->first();

        if (empty($user_course))
        {
            //if the user doesnt have this course insert course into db
            $user_has_course = new UserHasCourses();

            $user_has_course->users_id = $user_id;
            $user_has_course->courses_id = $course_id->courses_id;
            $user_has_course->status_id = 1;

            $user_has_course->save();
        }

        //get the assignment id and return the content view
        $assignment_contents = Assignment::find($assignment_content);

        return view('assignments/assignment_content', compact('assignment_contents', 'succes'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download($id){
        // Load the required assignment data
        $assignment_contents = Assignment::find($id);
        // Search for the file that needs to be downloaded
        $file = public_path() . $assignment_contents->file_location;
        // Add needed headers
        $headers = array(
            'Content-Type: text/plain'
        );
        // Return with the file
        return Response::download($file, "Test_opdracht.txt", $headers);
    }
}
