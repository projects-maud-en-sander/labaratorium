<?php

namespace App\Http\Controllers\Admin;


use App\Models\Assignment;
use App\Models\Course;
use App\Models\UserHasAssignment;
use App\Models\UserHasCourses;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;

class AdminMadeAssignmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        //check witch column was requested and set it to column_name
        $queries = [];
        $columns = [
            'name', 'course_name', 'assignment_name', 'updated_at'
        ];
        foreach ($columns as $column){
            if (request()->has($column)) {
                $column_name = $column;
                $queries[$column_name] = request($column);
            }
        }
        //if column_name is empty set column to course_name and queries to descending
        if (empty($column_name))
        {
            $column_name = 'course_name';
            $queries[$column_name] = 'desc';
        }

        //get all the fields needed for the table view inner join three tables:users, assignments and courses on each other
        $made_assignments = DB::table('user_has_assignments')
            ->join('users', 'users.id', '=', 'user_has_assignments.users_id')
            ->join('assignments', 'assignments.id', '=', 'user_has_assignments.assignments_id')
            ->join('courses', 'courses.id', '=', 'assignments.courses_id')
            //users.name, assignments.name and courses.name get the same name in the result which makes them not visible changed name wwith as
            ->select('assignments.name as assignment_name', 'assignments.id as assignment_id', 'users.name', 'user_has_assignments.updated_at', 'courses.name as course_name')
            ->orderBy($column_name , $queries[$column_name])
            ->get();

        $agent = new Agent();
        if ($agent->IsMobile()) {
            // Return the Courses view with the courses variable to use for looping through them
            return view('Mobile.admin.made_assignments', compact('made_assignments'));

        } else {
            // Return the Courses view with the courses variable to use for looping through them
            return view('admin.made_assignments', compact('made_assignments'));

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        //find the assignment that was made by the student so the teacher can download it
        $made_assignment_files = Assignment::find($id)->user_has_assignment;
        $assignment = Assignment::find($id);

        return view('admin.rating_create', compact('made_assignment_files', 'assignment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //check if form was submitted with the correct values
        $this->validate(request(),[
            'rating' => 'required',
        ]);

        //find the row in the table for the made assignment
        $user_assignment = UserHasAssignment::find($id);

        //update the values with the values from the form
        $user_assignment->rating = $request->rating;
        $user_assignment->feedback = $request->feedback;

        $user_assignment->update();

        if ($request->level == 1)
        {
            //get the the level of the current course
            $assignments = UserHasAssignment::where('id', $id)
                ->select('assignments_id')
                ->first();
            $course = Assignment::where('id', $assignments->assignments_id)
                ->select('courses_id')
                ->first();
            $level = Course::where('id', $course->courses_id)
                ->select('level')
                ->first();

            //get the user id
            $user_assignment = UserHasAssignment::where('id', $id)
                ->select('users_id')
                ->first();
            $user_level = User::where('id', $user_assignment)
                ->select('level')
                ->first();

            //insert the level into the db if it is greater than the user's current level.
            if ($user_level < $level->level + 1)
            {
                $level_new = $level->level + 1;


                User::where('id', $user_assignment->users_id)
                    ->update(['level' => $level_new]);
            }
            UserHasCourses::where('courses_id', '=', $course->courses_id)
                ->where('users_id', $user_assignment->users_id)
                ->update(['status_id' => 2]);
        }

        return redirect()->route('admin.made_assignments.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadClientFile($file) {
        return response()->download(storage_path('app/files/'. $file));
    }
}
