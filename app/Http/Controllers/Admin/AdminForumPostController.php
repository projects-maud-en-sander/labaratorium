<?php

namespace App\Http\Controllers\Admin;

use App\Models\ForumPost;
use App\Models\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminForumPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create($topic)
    {
        // Retrieve the topic to make a post in from the database.
        $topic = Topic::findOrFail($topic);

        // Return the forum post create view along with the retrieved topic.
        return view('admin.forum_post_create', compact('topic'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $topic)
    {
        // Validate the request.
        $request->validate([
            'title' => 'string|required|max:255',
            'message' => 'string|required',
        ]);

        // Create and store a new forum post.
        $forum_post = new ForumPost();

        $forum_post->title = $request->title;
        $forum_post->message = $request->message;
        $forum_post->users_id = Auth::user()->id;
        $forum_post->topics_id = $topic;

        $forum_post->save();

        // Redirect to the show page of the created forum post.
        return redirect()->route('admin.topics.posts.show', [$topic, $forum_post]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function show($topic, $forum_post)
    {
        // Find the topic the forum post belongs to.
        $find_topic = Topic::find($topic);

        // Generate a "path" to the current forum post.
        // This is accomplished by getting the topic the current forum post belongs to and then repeating this for
        // the topic you retrieved until you reach the base topic.
        // This is done so that you can easily navigate backwards.
        $path[] = $find_topic;
        while ($find_topic->topics_id != null) {
            $find_topic = Topic::find($find_topic->topics_id);
            $path[] = $find_topic;
        }

        $path = collect($path)->reverse();

        // Retrieve the forum post along with the user that created it from the database.
        $forum_post = ForumPost::find($forum_post)->load('users');

        // Get the sorting keyword.
        $sort = request('sort');

        // Use the sorting keyword to retrieve and sort the replies to the post.
        // It also retrieves the users that created the replies.
        switch ($sort) {
            case "message_asc":
                $replies = $forum_post->replies->load('users')->sortBy('message');
                break;
            case "message_desc":
                $replies = $forum_post->replies->load('users')->sortByDesc('message');
                break;
            case "name_asc":
                $replies = $forum_post->replies->load('users')->sortBy('name');
                break;
            case "name_desc":
                $replies = $forum_post->replies->load('users')->sortByDesc('name');
                break;
            case "created_at_asc":
                $replies = $forum_post->replies->load('users')->sortBy('created_at');
                break;
            case "created_at_desc":
                $replies = $forum_post->replies->load('users')->sortByDesc('created_at');
                break;
            default:
                $replies = $forum_post->replies->load('users');
                break;
        }

        // Get the user_id of the currently logged in user to display edit and delete buttons
        // on posts and replies that belong to that user.
        $user_id = Auth::user()->id;

        // Return the forum post show view along with:
        // - The topic the current forum post belongs to
        // - The current forum post
        // - The replies belonging to the current forum post
        // - The ID of the currently logged in user
        // - The path to the current forum post
        return view('admin.forum_post_show', compact('topic', 'forum_post', 'replies', 'user_id', 'path'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\View\View | \Illuminate\Http\RedirectResponse
     */
    public function edit($topic, $forum_post)
    {
        // Retrieve the forum post and the topic it belongs to from the database.
        $topic = Topic::findOrFail($topic);
        $forum_post = ForumPost::findOrFail($forum_post);

        // There is no check to see if the post belongs to the user here,
        // because admins have permission to edit and delete all forum posts.

        // Return the forum post edit view.
        return view('admin.forum_post_edit', compact('forum_post', 'topic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $topic
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $topic, $forum_post)
    {
        // Validate the request.
        $request->validate([
            'title' => 'required|string|max:255',
            'message' => 'required|string',
        ]);

        // Update the forum post.
        $forum_post = ForumPost::findOrFail($forum_post);
        $forum_post->title = $request->title;
        $forum_post->message = $request->message;

        $forum_post->update();

        // There is no check to see if the post belongs to the user here,
        // because admins have permission to edit and delete all forum posts.

        // Redirect back to the forum post show.
        return redirect()->route('admin.topics.posts.show', [$topic, $forum_post]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($topic, $forum_post)
    {
        // Retrieve the forum post from the database.
        $FindPost = ForumPost::findOrFail($forum_post);

        // There is no check to see if the post belongs to the user here,
        // because admins have permission to edit and delete all forum posts.

        // Delete the post.
        $FindPost->delete();

        // Redirect to the show of the topic that the forum post belonged to.
        return redirect()->route('admin.topics.show', compact('topic'));
    }
}
