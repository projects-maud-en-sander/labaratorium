<?php

namespace App\Http\Controllers\Admin;

use App\Models\ForumPost;
use App\Models\Reply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create($topic, $forum_post)
    {
        // Retrieve the forum post the user wants to reply to from the database.
        $forum_post = ForumPost::find($forum_post);

        // Return the reply create view along with:
        // - The forum post the user wants to reply to
        // - The topic that forum post belongs to
        return view('admin.reply_create', compact('topic', 'forum_post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $topic, $forum_post)
    {
        // Validate the request.
        $request->validate( array(
            'message' => 'required|string',
        ));

        // Create and store a new reply.
        $reply = new Reply;

        $reply->message = $request->message;
        $reply->forum_posts_id = $forum_post;
        $reply->users_id = Auth::id();

        $reply->save();

        // Redirect to the show of the forum post the reply belongs to.
        return redirect()->route('admin.topics.posts.show', [$topic, $forum_post]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $reply
     * @return \Illuminate\View\View
     */
    public function edit($topic, $forum_post, $reply)
    {
        // Retrieve the reply and the forum post it belongs to from the database.
        $forum_post = ForumPost::findOrFail($forum_post);
        $reply = Reply::findOrFail($reply);

        // Return the reply edit view.
        return view('admin.reply_edit', compact('topic', 'forum_post', 'reply'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $topic
     * @param  int  $forum_post
     * @param  int  $reply
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $topic, $forum_post, $reply)
    {
        // Validate the request.
        $request->validate( array(
            'message' => 'required|string',
        ));

        // Retrieve the reply from the database.
        $reply = Reply::findOrFail($reply);

        // There is no check to see if the reply belongs to the user here,
        // because admins have permission to edit and delete all replies.

        // Update the reply.
        $reply->message = $request->message;

        $reply->save();

        // Redirect back to the forum post the reply belongs to.
        return redirect()->route('admin.topics.posts.show', [$topic, $forum_post]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($topic, $forum_post, $reply)
    {
        // Retrieve the reply from the database.
        $reply = Reply::findOrFail($reply);

        // There is no check to see if the reply belongs to the user here,
        // because admins have permission to edit and delete all replies.

        // Delete the reply
        $reply->delete();

        // Redirect back to the forum post the reply belonged to.
        return redirect()->route('admin.topics.posts.show', [$topic, $forum_post]);
    }
}
