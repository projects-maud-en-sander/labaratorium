<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateAssignmentRequest;
use App\Models\Assignment;
use App\Models\Course;
use App\Models\EndAssignment;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Jenssegers\Agent\Agent;

class AdminAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $course
     * @return \Illuminate\View\View
     */
    public function index($course)
    {
        // Retrieve all assignments that belong to the selected course from the database.
        $assignments = Assignment::all()->where('courses_id', '=', $course);

        // Return the assignment index view along with the course and assignments.
        return view('admin.assignments', compact('course', 'assignments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $course
     * @return \Illuminate\View\View
     */
    public function create($course)
    {
        // Return the assignment create view.
        return view('admin.assignment_create', compact('course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateAssignmentRequest $request, $course)
    {
        // Validate the request using CreateAssignmentRequest.
        $request->validated();

        // Save the uploaded files if they are not empty.
        if ($request->file != Null) {
            $request->file->storeAs('files', $request->file->getClientOriginalName());
        }
        if ($request->video != Null) {
            $request->video->storeAs('videos', $request->video->getClientOriginalName());
        }

        // Create and save a new forum topic belonging to the created assignment.
        $topic = new Topic;
        $topic->name = $request->name;
        $topic->topics_id = Course::find($course)->topics_id;

        $topic->save();

        // Create and save a new assignment.
        $assignment = new Assignment();
        $assignment->name = $request->name;
        $assignment->description = $request->description;
        $assignment->courses_id = $course;
        $assignment->topics_id = $topic->id;

        if ($request->end_assignment_check == 1)
        {
            $assignment->final_assignment = 1;
        }
        if ($request->file != Null)
        {
            $assignment->file_location = $request->file->getClientOriginalName();
        }
        if ($request->video != Null)
        {
            $assignment->video_location = $request->video->getClientOriginalName();
        }

        $assignment->save();

        // If the assignment is an end assignment, create a new database entry in the EndAssignment table.
        if ($request->end_assignment_check == 1) {
            $end_assignment = new EndAssignment();
            $end_assignment->assignments_id = $assignment->id;

            $end_assignment->save();
        }

        // Redirect back to the assignment index.
        return redirect()->route('admin.courses.assignments.index', compact('course'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $course
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function edit($course, $id)
    {
        // Retrieve the assignment from the database.
        $assignment = Assignment::find($id);
        $end_assignment = EndAssignment::all()->where('assignments_id', '=', $assignment->id);

        // Instantiate a new agent for checking if the page is being viewed from a mobile device or not.
        $agent = new Agent();

        // If the page is being viewed from mobile, return the mobile edit view
        if ($agent->IsMobile())
        {
            return view('mobile.admin.assignment_edit', compact('course', 'assignment', 'end_assignment'));
        }
        // If not, return the regular edit view.
        else
        {
            return view('admin.assignment_edit', compact('course', 'assignment', 'end_assignment'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $course
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $course, $id)
    {
        // Validate the request.
        $request->validate([
            'name' => 'required|max:255',
            'end_assignment_check' => 'int'
        ]);

        // Save the uploaded files if they are not empty.
        if ($request->file != Null) {
            $request->file->storeAs('files', $request->file->getClientOriginalName());
        }
        if ($request->video != Null) {
            $request->video->storeAs('videos', $request->video->getClientOriginalName());
        }

        // Retrieve and update the assignment.
        $assignment = Assignment::find($id);

        $assignment->name = $request->name;
        $assignment->description = $request->description;

        if ($request->file != Null) {
            $assignment->file_location = $request->file->getClientOriginalName();
        }
        if ($request->video != Null) {
            $assignment->video_location = $request->video->getClientOriginalName();
        }

        $assignment->update();

        // Retrieve and update the forum topic belonging to the assignment.
        $topic = Topic::find($assignment->topics_id);

        if ($topic != null) {
            $topic->name = $request->name;

            $topic->update();
        }

        // Retrieve the end assignment entry belonging to the assignment from the database
        $end_assignment = EndAssignment::where('assignments_id', '=', $assignment->id)->first();

        // If the end assignment checkmark wasn't checked and the end assignment entry exists,
        // delete the end assignment database entry.
        if ($request->end_assignment_check == 0 && $end_assignment != null) {
            $end_assignment->delete();
        }

        // If the end assignment checkmark was checked and the end assignment entry doesn't exist,
        // create a new end assignment entry.
        else if ($request->end_assignment_check == 1 && $end_assignment == null) {
            $end_assignment = new EndAssignment();
            $end_assignment->assignments_id = $assignment->id;

            $end_assignment->save();
        }

        // Redirect back to the assignments index page.
        return redirect()->route('admin.courses.assignments.index', compact('course'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $course
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($course, $id)
    {
        // Retrieve the assignment and the topic that belongs to it from the database.
        $FindAssignment = Assignment::findOrFail($id);
        $FindTopic = Topic::findOrFail($FindAssignment->topics_id);

        // Delete the assignment and the topic from the database.
        $FindAssignment->delete();
        $FindTopic->delete();

        // Redirect back to the assignments index page.
        return redirect()->route('admin.courses.assignments.index', compact('course'))->with('success', 'Opdracht verwijderd');
    }
}
