<?php

namespace App\Http\Controllers\Admin;

use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // Retrieve all notifications from the database.
        $notifications = Notification::all();

        // Return the notification index view along with the notifications.
        return view('admin.notifications', compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // Return the notification create view.
        return view('admin.notification_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // Validate the request.
        $request->validate([
            'title' => 'string|required|max:255',
            'message' => 'string|required',
        ]);

        // Create and save a new notification
        $notification = new Notification;

        $notification->title = $request->title;
        $notification->message = $request->message;

        $notification->save();

        // Redirect back to the notification index.
        return redirect()->route('admin.notifications.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($notification)
    {
        // Retrieve the selected notification from the database.
        $notification = Notification::findOrFail($notification);

        // Return the notification edit view along with the notification.
        return view('admin.notification_edit', compact('notification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $notification)
    {
        // Validate the request.
        $request->validate([
            'title' => 'string|required|max:255',
            'message' => 'string|required',
        ]);

        // Retrieve the notification from the database.
        $notification = Notification::find($notification);

        // Update the notification.
        $notification->title = $request->title;
        $notification->message = $request->message;

        $notification->update();

        // Redirect back to the notification index.
        return redirect()->route('admin.notifications.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($notification)
    {
        // Retrieve the selected notification from the database.
        $notification = Notification::findOrFail($notification);

        // Delete the notification.
        $notification->delete();

        // Redirect back to the notification index.
        return redirect()->route('admin.notifications.index');
    }
}
