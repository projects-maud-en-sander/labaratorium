<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateCourseRequest;
use App\Models\Topic;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Course;
use Illuminate\View\View;
use Jenssegers\Agent\Agent;
use Ramsey\Collection\Collection;

class AdminCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // Get the sorting keyword.
        $sort = request('sort');

        // Use the sorting keyword to retrieve and sort the courses.
        if ($sort == 'name_asc'){
            $courses = Course::all()->sortBy('name');
        }elseif($sort == 'name_desc'){
            $courses = Course::all()->sortByDesc('name');
        }elseif($sort == 'date_asc'){
            $courses = Course::all()->sortBy('created_at');
        }elseif($sort == 'date_desc'){
            $courses = Course::all()->sortByDesc('created_at');
        }else{
            $courses = Course::all();
        }

        // Return the courses index view along with the courses.
        return view('admin.courses', [
            'courses' => $courses,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // Return the course create view.
        return View('admin.courses_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // Validate the request.
        $request->validate( array(
            'name' => 'required',
            'description' => 'required',
            'level' => 'required'
        ));

        // Create and store a new forum topic for the course.
        $topic = new Topic;
        $topic->name = $request->name;

        $topic->save();

        // Create and store a new course.
        $courses = new Course;
        $courses->name = $request->name;
        $courses->description = $request->description;
        $courses->level = $request->level;
        $courses->topics_id = $topic->id;

        $courses->save();

        // Redirect back to the courses index page.
        return redirect(route('admin.courses.index'))->with('success', 'Cursus aangemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Collection $course
     * @return View
     */
    public function edit($id)
    {
        // Retrieve the course from the database.
        $course = Course::find($id);

        // Instantiate a new agent for checking if the page is being viewed from a mobile device or not.
        $agent = new Agent();

        // If the page is being viewed from mobile, return the mobile edit view
        if ($agent->IsMobile())
        {
            return view('mobile.admin.course_edit', compact('course'));
        }
        // If not, return the regular edit view.
        else
        {
            return view('admin.course_edit', compact('course'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCourseRequest  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update(UpdateCourseRequest $request, $id)
    {
        // Validate the request using the UpdateCourseRequest.
        $data = $request->validated();

        // Retrieve and update the course.
        $course = Course::find($id);

        $course->name = $data['name'];
        $course->description = $data['description'];
        $course->level = $data['level'];

        $course->update();

        // Retrieve and update the topic belonging to the course.
        $topic = Topic::find($course->topics_id);
        $topic->name = $data['name'];

        $topic->update();

        // Redirect back to the courses index.
        return redirect()->route('admin.courses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        // Retrieve the course and the topic that belongs to it from the database.
        $FindCourse = Course::findOrFail($id);
        $FindTopic = Topic::findOrFail($FindCourse->topics_id);

        // Delete the course and the topic from the database.
        $FindCourse->delete();
        $FindTopic->delete();

        // Redirect back to the courses index page.
        return redirect()->route('admin.courses.index')->with('success', 'Cursus verwijderd');
    }
}
