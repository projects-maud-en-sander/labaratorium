<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //check if the user is an admin
        //if the user is not return to home
        if (Auth::user() &&  Auth::user()->roles_id == 3) {
            return $next($request);
        }

        return redirect('/home');
    }
}
