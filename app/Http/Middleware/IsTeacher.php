<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsTeacher
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        //check if the user is a teacher
        //else return to home
        if (Auth::user()) {
            if (Auth::user()->roles_id == 2 || Auth::user()->roles_id == 3) {

                return $next($request);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }

    }
}
