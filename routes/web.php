<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

//check if the user is logged in
Route::group(['middleware' => ['auth']], function () {

    //put admin. in front of every admin route
    Route::prefix('admin')->name('admin.')->group(function () {

        //admin-only routes
        Route::group(['middleware' => ['is_admin']], function () {
            Route::resource('roles_update', 'SuperAdmin\AdminRoleController');
        });

        //admin/teacher-only routes
        Route::group(['middleware' => ['is_teacher']], function () {
            //course management routes
            Route::resource('courses', 'Admin\AdminCourseController');

            //assignment management routes
            Route::resource('courses.assignments', 'Admin\AdminAssignmentController');
            Route::resource('made_assignments', 'Admin\AdminMadeAssignmentsController');
            Route::get('/made_assignments/download/{path}/', 'Admin\AdminMadeAssignmentsController@downloadClientFile')->name('download-file');

            //notification management routes
            Route::resource('notifications', 'Admin\AdminNotificationController')->except('show');

            //forum management routes
            Route::resource('topics', 'Admin\AdminTopicController');
            Route::resource('topics.posts', 'Admin\AdminForumPostController')->except('index');
            Route::resource('topics.posts.replies', 'Admin\AdminReplyController')->except('index', 'show');
        });
    });

    //course routes
    Route::resource('courses', 'CourseController');
    Route::get('courses_status', 'CourseController@index')->name('courses');

    //assignment routes
    Route::resource('assignments', 'AssignmentController');
    Route::resource('download', 'AssignmentDownloadController');
    Route::get('/download/download/{path}/', 'AssignmentDownloadController@downloadClientFile')->name('download-assignment-file');

    //reviewed assignments
    Route::resource('reviewed_assignments', 'ReviewedAssignmentController');

    //profile page for user
    Route::resource('profile', 'ProfileController');

    //forum routes
    Route::prefix('forum')->group(function () {
        Route::get('/', 'TopicController@index')->name('forum');
        Route::get('/my-posts', 'ForumPostController@index')->name('my-posts');
        Route::resource('topics', 'TopicController');
        Route::resource('topics.posts', 'ForumPostController')->except('index');
        Route::resource('topics.posts.replies', 'ReplyController')->except('index', 'show');
    });
});



